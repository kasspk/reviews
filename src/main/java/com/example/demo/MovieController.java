package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping("/movie")
public class MovieController {
    @Autowired
    private MovieRepository repository;
    @GetMapping("/list")
    private String getAll(Model model){
        model.addAttribute("movies", repository.findAll());
        return "movie_list";
    }
    @GetMapping(path = {"/add", "edit/{id}"})
    private String addForm(@PathVariable("id") Optional<Long> id, Model model){
        if(id.isPresent()){
            model.addAttribute("movie", repository.findById(id.get()));
        }else{
            model.addAttribute("movie", new Movie());
        }
        return "add_edit_movie";
    }
    @PutMapping("/addEdit")
    private String insertOrUpdate(Movie movie){
            Optional<Movie> movieOptional = repository.findById(movie.getId());
            if(movieOptional.isPresent()) {
                Movie editMovie = movieOptional.get();
                editMovie.setTitle(movie.getTitle());
                repository.save(editMovie);
            }

        return "redirect:/movie/list";
    }

}
