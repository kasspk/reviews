package com.example.demo;

import javax.persistence.*;


    @Entity
    @Table(name = "movies")
    public class Movie {
        @Id
        @GeneratedValue(strategy= GenerationType.AUTO)
        private Long id;
        private String title;

        public Long getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

